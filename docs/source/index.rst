.. pytbc documentation master file, created by
   sphinx-quickstart on Tue Feb 18 23:02:35 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

=======================================
pytbc - Tree-based clustering in Python
=======================================

.. role:: raw-html(raw)
   :format: html

**pytbc** contains Python bindings to the tree-based clustering algorithm 
by Vitalis and Caflisch [Vitalis2012]_ implemented in `Campari <http://campari.sourceforge.net/>`_.
:raw-html:`<br />`
The Fortran to Python interface is generated with `f90wrap <https://github.com/jameskermode/f90wrap>`_ 
to have access to derived types and then compiled with `f2py <https://docs.scipy.org/doc/numpy/f2py/>`_.

Extensions
----------

Campari **pytbc** bindings are for the moment limited to the tree-based clustering 
algorithm with dihedral or Euclidean distance (see :ref:`apireference`). More advanced and complete
functionalities can be accessed by directly running `Campari <http://campari.sourceforge.net/>`_ 
or by using the `CampaRi R package <https://gitlab.com/CaflischLab/CampaRi>`_, a 
complete wrapper of Campari written in R. The user can refer to the documentation
of these projects for more in-depth explanations of the algorithm and use cases.

Citations
---------

Please cite [Vitalis2012]_ if you use **pytbc** in your work.
This project was realized as part of an `E-CAM <https://www.e-cam2020.eu/>`_ 
Extended Software Development workshop.
:raw-html:`<br />`
Contributions to this project were given by C. Langini (main development), M. Bacci (preliminary work on the Fortran code) and 
A. Vitalis (vision and support) from the `Caflisch lab <http://www.biochem-caflisch.uzh.ch/>`_.
For Campari installation, this project makes use of the Campari configuration file developed by D. Garolini available at `https://gitlab.com/CaflischLab/debcampari <https://gitlab.com/CaflischLab/debcampari>`_).

.. [Vitalis2012] A. Vitalis and A. Caflisch. Efficient Construction of Mesostate Networks from Molecular Dynamics Trajectories. 
   J. Chem. Theory Comput. 8 (3), 1108-1120 (2012) `DOI <https://pubs.acs.org/doi/abs/10.1021/ct200801b>`_

Contents:
=========

.. toctree::
   :maxdepth: 2

   gettingstarted
   tutorials
   apireference



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
