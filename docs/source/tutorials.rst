.. _tutorials:

Tutorials
=========

Here are some example use cases for the tree-based clustering algorithm.

.. toctree::
   :maxdepth: 1

   notebooks/example_TBC.ipynb