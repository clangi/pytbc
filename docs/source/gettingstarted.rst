Getting started
===============

Installation of **pytbc** requires a valid installation of Campari
software library to link against.

Prerequisites
-------------

In order to compile Campari we recommend to install blas, lapack
and fftw3 libraries. 
On Debian systems run the following:

.. code-block:: bash

    sudo apt-get update
    sudo apt-get install -y libblas-dev liblapack-dev libfftw3-dev 

The easiest way to install Campari is by using the configuration file distributed at
`https://gitlab.com/CaflischLab/debcampari <https://gitlab.com/CaflischLab/debcampari>`_.
Note that the Campari library has to be compiled with flag ``-fPIC`` to be able to be 
linked to successfully. This would require to supply the additional flag when configuring
or, better, editing a ``Makefile.local`` file to override the compilation flags.

We provide a script which does that and achieves a basic Campari serial installation:

.. code-block:: bash

    git clone https://gitlab.com/clangi/pytbc
    cd pytbc
    bash install_campari_lib.sh

The script installs Campari in the repository folder ``./pytbc/inst/`` (default).
Campari compilation will generate the static library ``libcampari.a`` to which 
pytbc is linked. The library should be in the LD\_LIBRARY\_PATH or should
be added to ``setup.py`` to the list of ``extra_link_args`` (it is already there if Campari
was compiled in the default repository folder).

Installation 
------------

The Fortran-Python interface (``pyclustering.py``) has been preemptively generated 
with f90wrap. Upon modifications to Fortran source files, it can be regenerated as follows;
make sure that ``libcampari.a`` is in the LD\_LIBRARY\_PATH before running the script.

.. code-block:: bash 

    cd src
    bash wrapit_f90wrap.sh

Upon successfull installation of Campari (and, if needed, 
generation of the ``pyclustering.py`` module) pytbc can be installed 
with pip:

.. code-block:: bash

    pip install -e .

Usage
-----

In order to use **pytbc** it is enough to import it in Python. 
After initializing a ``TreeBased`` object, it can be used to cluster 
data with the ``fit`` method.

.. code-block:: python

    import pytbc 
    tbc = pytbc.TreeBased()
    y_pred = tbc.fit(X)

Example use cases are provided in the :ref:`Tutorials` section.