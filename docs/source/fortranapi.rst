Fortran API reference
=====================

.. f:autotype:: birch_param_type

.. f:autofunction:: birch_wrapper

.. f:autofunction:: clean_after_fit

.. f:autofunction:: gen_clu_out