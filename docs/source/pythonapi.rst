Python API reference
====================

.. automodule:: pytbc

Detailed module reference:

.. automodule:: pytbc.tree_based
    :members: