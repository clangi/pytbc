import pytest
import warnings

import numpy as np
import pytbc
from pytbc.tree_based import TreeBased

class TestTreeBased(object):
    '''Testing class for tree based
    '''

    def test_dopix_not_implemented(self):
        '''
        '''
        tbc = TreeBased(dopix=True)
        X = np.random.rand(2,5)
        with pytest.raises(NotImplementedError):
            tbc.fit(X)
    
    # test for _check_input_pars()
    def test_cdis_not_implemented(self):
        import random
        cdis_range = [x for x in range(1, 100) if x not in [1,3,7]]
        for i in range(10):
            x = random.choice(cdis_range)
            with pytest.raises(ValueError):
                _ = TreeBased(cdis_crit=x)

    # test for _check_input_pars()
    def test_cradius_larger_than_cmaxrad(self):
        with pytest.warns(Warning):
            _ = TreeBased(cmaxrad=5.0, cradius=10.0)

    def test_init_birch_pars(self):
        tbc = TreeBased(nlevels=5)
        assert tbc.params.c_nhier == 5
        assert type(tbc.params) is pytbc.pyclustering.Pyclustering.birch_param_type

    def test_fit_output(self):
        '''Smoke test for fit.
        
        Checks that the shape of output file is correct.
        '''
        tbc = TreeBased()
        n_points = np.random.randint(low=1, high=20)
        X = np.random.rand(2, n_points)
        y_pred = tbc.fit(X)
        assert y_pred.shape == (n_points,1)
    
    def test_get_clu_statistics(self):
        '''Smoky test for get_clu_statistics.
        '''
        tbc = TreeBased(cradius=8.0, cmaxrad=100.0)
        n_points = 100
        X = np.concatenate((np.random.rand(1, n_points), 
                            np.random.rand(1, n_points) + 50.0), axis=1)
        print(X.shape)
        y_pred = tbc.fit(X)

        print(tbc.nclus)
        print(tbc.nsnaps_by_clu)
        assert np.array_equal(tbc.nsnaps_by_clu, 
                              np.array([n_points, n_points]))
        assert tbc.centroids.shape == (2,)
        assert tbc.radii.shape == (2,)
        assert tbc.diameters.shape == (2,)

    def test_an_int_please(self):
        tbc = TreeBased()
        assert type(tbc.an_int_please()) is int 

    # This is an example from tutorial used as system test:
    def test_blobs_cdis_7(self):
        from sklearn.datasets import make_blobs
        # make blobs:
        n_samples = 1500
        random_state = 170
        X, y = make_blobs(n_samples=n_samples,
                        random_state=random_state, cluster_std=0.5)

        tbc_blobs = TreeBased(cdis_crit=1, nlevels=4, cradius=2, cmaxrad=5)
        y_pred = tbc_blobs.fit(np.transpose(X))

        # check that we correctly assign the dots to the three blobs:
        np.testing.assert_equal(np.unique(y_pred[np.where(y == 0), 0]).size, 1)
        np.testing.assert_equal(np.unique(y_pred[np.where(y == 1), 0]).size, 1)
        np.testing.assert_equal(np.unique(y_pred[np.where(y == 2), 0]).size, 1)
        # check that we actually find 3 clusters:
        np.testing.assert_equal(np.unique(y_pred).size, 3)

    # This is an example from tutorial used as system test:
    def test_polar_blobs_cdis_1_3(self):
        # make polar blobs:
        n_samples = 700
        # building two blobs (one centered around 0 degrees, the other centered around 180 degrees)
        blob_0 = np.random.normal(loc=0.0, scale=5.0, size=n_samples)
        blob_180 = np.concatenate((np.random.normal(loc=180.0, scale=5.0, size=int(n_samples/2)),
                                np.random.normal(loc=-180.0, scale=5.0, size=int(n_samples/2))))
        X = np.concatenate((blob_0, blob_180)).reshape((1, n_samples*2))
        X = (((np.round(X)+180) % 360)-180)  # convert to interval [-180, +180]
        # fits:
        tbc_angle = TreeBased(cdis_crit=1, nlevels=4, cradius=45, cmaxrad=90)
        y_pred_angle = tbc_angle.fit(X)
        tbc_angle2 = TreeBased(cdis_crit=3, nlevels=4, cradius=0.5, cmaxrad=10)
        y_pred_angle2 = tbc_angle2.fit(X)

        # check that we correctly assign the dots to the three blobs:
        np.testing.assert_equal(np.unique(y_pred_angle[0:n_samples,0]).size, 1)
        np.testing.assert_equal(np.unique(y_pred_angle[n_samples:(2*n_samples),0]).size, 1)
        np.testing.assert_equal(np.unique(y_pred_angle2[0:n_samples,0]).size, 1)
        np.testing.assert_equal(np.unique(y_pred_angle2[n_samples:(2*n_samples),0]).size, 1)
        # check that we actually find 3 clusters:
        np.testing.assert_equal(np.unique(y_pred_angle).size, 2)
        np.testing.assert_equal(np.unique(y_pred_angle2).size, 2)

    # The real system test should be a 1-to-1 test with Campari!
    # This is still missing for the moment!
