"""

.. module:: tree_based

"""
# Authors: Cassiano Langini <cassiano.langini@gmail.com>

import warnings
import numpy as np
from .pyclustering import pyclustering as tb 
# import _pyclustering as _tb
# note that pyclustering.pyclustering is an object of type Pyclustering
# hence also tb is an object of type Pyclustering

class TreeBased(object):
    """Wrapper class for the tree-based clustering algorithm.

    Creating an istance of `TreeBased` will set all the parameters necessary
    for the clustering.

    Attributes
    ----------
    cmaxrad : float
        largest radius for clustering (the one used at the root level)
    cradius : float
        minimum radius for clustering (the one used at the leaf level)
    nlevels : int
        number of levels in the tree
    cdis_crit : int 
        cluster distance. Possible choices are the following:
            1 : dihedral distance.
            3 : dihedral distance with sin and cos components.
            7 : Euclidean distance.
    nreplicas : int 
        number of replicas if clustering a multi-replica trajectory.
    nreplicas_sampled : int 
        number of sampled replicas in the original trajectory.
    dopix : bool 
        If `True` it calculates the Progress Index (with short spanning tree)
        based on the clustering.
    doheuristic : bool
        Calculate resampling heuristics for PIGS simulations.
    c_multires : int 
        number of additional levels of the tree to refine. Defaults to 0 (only
        leaf level is refined).
    re_freq : int 
        Replica Exchange frequency for multi-replica trajectories.
    good_pigs : int
        Number of copies protected from reseeding in PIGS simulation.
    cprogindrmax : int 
        Maximum number of search attempts when building the Progress Index.
    cprogpwidth : int
        window width for localized cut.
    cprogrdepth : int 
        Number of levels to traverse back on the clustering tree when building 
        the Progress Index.  

    """
    def __init__(self,cmaxrad=10000, cradius=9999.0, nlevels=8,
                 cdis_crit=7, nreplicas=1, nreplicas_sampled=1,
                 c_multires = 0, 
                 re_freq = 1, good_pigs=1,
                 cprogindrmax = 50,
                 cprogpwidth = 1000,
                 cprogrdepth = 0, 
                 dopix=False, doheuristic=False):

        self.cmaxrad = cmaxrad
        self.cradius = cradius
        self.nlevels = nlevels
        self.cdis_crit = cdis_crit
        self.nreplicas = nreplicas
        self.nreplicas_sampled = nreplicas_sampled  
        self.dopix = dopix
        self.doheuristic = doheuristic
        self.c_multires = c_multires
        self.re_freq = re_freq 
        self.good_pigs = good_pigs
        self.cprogindrmax = cprogindrmax
        self.cprogpwidth = cprogpwidth
        self.cprogrdepth = cprogrdepth 

        self._check_input_pars() # check inputs

        self.params = self._init_birch_pars()

        self.y = None # here we will store the clustering results
        
        self.nsnaps_by_clu = None
        self.centroids = None 
        self.radii = None 
        self.diameters = None
        self.nclus = None

    
    def _init_birch_pars(self): 
        # Consider reorganizing to avoid code duplication
        pars = tb.birch_param_type()
        pars.cmaxrad = self.cmaxrad 
        pars.cradius = self.cradius 
        pars.cdis_crit = self.cdis_crit   
        pars.dopix = self.dopix 
        pars.c_multires = self.c_multires 
        pars.c_nhier = self.nlevels
        pars.doheuristic = self.doheuristic
        pars.re_freq = self.re_freq 
        pars.good_pigs = self.good_pigs 
        pars.cprogindrmax = self.cprogindrmax
        pars.cprogpwidth = self.cprogpwidth
        pars.cprogrdepth = self.cprogrdepth

        return pars
    
    def _check_input_pars(self):
        # check input arguments
        if self.cmaxrad < self.cradius:
            warnings.warn('cmaxrad cannot be smaller than cradius. Swapping them.', 
                          Warning)
            tmp = self.cmaxrad
            self.cmaxrad = self.cradius 
            self.cradius = tmp

        if self.cdis_crit not in [1,3,7]:
            raise ValueError('Only allowed values (so far) for cdis_crit are 1,3,7')

    def fit(self, X):
        """Fit the data with the clustering algorithm

        This runs the clustering algorithm on the provided data.

        Parameters
        ----------
        X
            a 2D numpy array containing the data to cluster.
            The shape of `X` should be (number of features)x(number of snapshots).
        
        Returns
        -------
        np.array
            a numpy array containing the cluster index for each snapshot.

        """
        nfeats = X.shape[0]
        nsnaps = X.shape[1]
        
        # self.labels_ = 
        if (self.dopix == False):
            tb.birch_wrapper(self.params, self.nreplicas, 
                             self.nreplicas_sampled,
                             X, nfeats, nsnaps)
            # preallocate output:
            self.y = np.zeros((nsnaps, self.c_multires+1), dtype=np.float64, 
                              order='F')
            tb.gen_clu_out(self.y, nsnaps, self.c_multires+1)

            # get cluster statistics (leaf level):
            self.nclus =  np.max(self.y[:,0]).astype(np.int32)
            self.nsnaps_by_clu = np.zeros(self.nclus, dtype=np.int32)
            self.centroids = np.zeros(self.nclus, dtype=np.int32)
            self.radii = np.zeros(self.nclus, dtype=np.float64)
            self.diameters = np.zeros(self.nclus, dtype=np.float64)

            tb.gen_clu_statistics(self.nsnaps_by_clu, self.centroids, 
                                  self.radii, self.diameters, self.nclus)
            # clean:
            tb.clean_after_fit()

            return self.y

        else:
            raise NotImplementedError
            # tb.birch_wrapper(self.params,self.nreplicas,self.nreplicas_sampled,X,nfeats,nsnaps)
            # self.y = np.zeros((nsnaps,self.c_multires+1),dtype=np.float64, order='F')
            # tb.gen_clu_out(self.y,nsnaps,self.c_multires+1)

            # self.progind = np.zeros((nsnaps,20),dtype=np.float64, order='F')
            # tb.get_progind(self.progind, nsnaps,20) 
            
            # if(self.doheuristic):
            #     self.reseeding = np.zeros(self.nreplicas_sampled, dtype = np.int32)
            #     tb.get_nmap(self.reseeding, self.nreplicas_sampled)

            # return self.y, self.progind
    
    def an_int_please(self):
        """Returns an int

        Dummy function to test the wrapper.

        Returns
        -------
        int
            a random int

        """
        return tb.give_me_an_int()

    # def a_matrix_please(self):
    #     return tb.give_me_an_allocatable()

