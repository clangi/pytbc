from __future__ import print_function, absolute_import, division
import _pyclustering
import f90wrap.runtime
import logging

class Pyclustering(f90wrap.runtime.FortranModule):
    """
    Module pyclustering
    
    
    Defined at birch_2wrap.F90 lines 2-579
    
    """
    @f90wrap.runtime.register_class("pyclustering.birch_param_type")
    class birch_param_type(f90wrap.runtime.FortranDerivedType):
        """
        Type(name=birch_param_type)
        
        
        Defined at birch_2wrap.F90 lines 6-20
        
        """
        def __init__(self, handle=None):
            """
            self = birch_param_type()
            
            
            Defined at birch_2wrap.F90 lines 6-20
            
            
            Returns
            -------
            this : birch_param_type
            	Object to be constructed
            
            
            Automatically generated constructor for birch_param_type
            """
            f90wrap.runtime.FortranDerivedType.__init__(self)
            result = _pyclustering.f90wrap_birch_param_type_initialise()
            self._handle = result[0] if isinstance(result, tuple) else result
        
        def __del__(self):
            """
            Destructor for class birch_param_type
            
            
            Defined at birch_2wrap.F90 lines 6-20
            
            Parameters
            ----------
            this : birch_param_type
            	Object to be destructed
            
            
            Automatically generated destructor for birch_param_type
            """
            if self._alloc:
                _pyclustering.f90wrap_birch_param_type_finalise(this=self._handle)
        
        @property
        def cmaxrad(self):
            """
            Element cmaxrad ftype=real(kind=8) pytype=float
            
            
            Defined at birch_2wrap.F90 line 9
            
            """
            return _pyclustering.f90wrap_birch_param_type__get__cmaxrad(self._handle)
        
        @cmaxrad.setter
        def cmaxrad(self, cmaxrad):
            _pyclustering.f90wrap_birch_param_type__set__cmaxrad(self._handle, cmaxrad)
        
        @property
        def cradius(self):
            """
            Element cradius ftype=real(kind=8) pytype=float
            
            
            Defined at birch_2wrap.F90 line 10
            
            """
            return _pyclustering.f90wrap_birch_param_type__get__cradius(self._handle)
        
        @cradius.setter
        def cradius(self, cradius):
            _pyclustering.f90wrap_birch_param_type__set__cradius(self._handle, cradius)
        
        @property
        def c_nhier(self):
            """
            Element c_nhier ftype=integer  pytype=int
            
            
            Defined at birch_2wrap.F90 line 11
            
            """
            return _pyclustering.f90wrap_birch_param_type__get__c_nhier(self._handle)
        
        @c_nhier.setter
        def c_nhier(self, c_nhier):
            _pyclustering.f90wrap_birch_param_type__set__c_nhier(self._handle, c_nhier)
        
        @property
        def cdis_crit(self):
            """
            Element cdis_crit ftype=integer  pytype=int
            
            
            Defined at birch_2wrap.F90 line 12
            
            """
            return _pyclustering.f90wrap_birch_param_type__get__cdis_crit(self._handle)
        
        @cdis_crit.setter
        def cdis_crit(self, cdis_crit):
            _pyclustering.f90wrap_birch_param_type__set__cdis_crit(self._handle, cdis_crit)
        
        @property
        def c_multires(self):
            """
            Element c_multires ftype=integer  pytype=int
            
            
            Defined at birch_2wrap.F90 line 13
            
            """
            return _pyclustering.f90wrap_birch_param_type__get__c_multires(self._handle)
        
        @c_multires.setter
        def c_multires(self, c_multires):
            _pyclustering.f90wrap_birch_param_type__set__c_multires(self._handle, \
                c_multires)
        
        @property
        def dopix(self):
            """
            Element dopix ftype=logical pytype=bool
            
            
            Defined at birch_2wrap.F90 line 14
            
            """
            return _pyclustering.f90wrap_birch_param_type__get__dopix(self._handle)
        
        @dopix.setter
        def dopix(self, dopix):
            _pyclustering.f90wrap_birch_param_type__set__dopix(self._handle, dopix)
        
        @property
        def doheuristic(self):
            """
            Element doheuristic ftype=logical pytype=bool
            
            
            Defined at birch_2wrap.F90 line 15
            
            """
            return _pyclustering.f90wrap_birch_param_type__get__doheuristic(self._handle)
        
        @doheuristic.setter
        def doheuristic(self, doheuristic):
            _pyclustering.f90wrap_birch_param_type__set__doheuristic(self._handle, \
                doheuristic)
        
        @property
        def good_pigs(self):
            """
            Element good_pigs ftype=integer  pytype=int
            
            
            Defined at birch_2wrap.F90 line 16
            
            """
            return _pyclustering.f90wrap_birch_param_type__get__good_pigs(self._handle)
        
        @good_pigs.setter
        def good_pigs(self, good_pigs):
            _pyclustering.f90wrap_birch_param_type__set__good_pigs(self._handle, good_pigs)
        
        @property
        def re_freq(self):
            """
            Element re_freq ftype=integer  pytype=int
            
            
            Defined at birch_2wrap.F90 line 17
            
            """
            return _pyclustering.f90wrap_birch_param_type__get__re_freq(self._handle)
        
        @re_freq.setter
        def re_freq(self, re_freq):
            _pyclustering.f90wrap_birch_param_type__set__re_freq(self._handle, re_freq)
        
        @property
        def cprogindrmax(self):
            """
            Element cprogindrmax ftype=integer  pytype=int
            
            
            Defined at birch_2wrap.F90 line 18
            
            """
            return _pyclustering.f90wrap_birch_param_type__get__cprogindrmax(self._handle)
        
        @cprogindrmax.setter
        def cprogindrmax(self, cprogindrmax):
            _pyclustering.f90wrap_birch_param_type__set__cprogindrmax(self._handle, \
                cprogindrmax)
        
        @property
        def cprogpwidth(self):
            """
            Element cprogpwidth ftype=integer  pytype=int
            
            
            Defined at birch_2wrap.F90 line 19
            
            """
            return _pyclustering.f90wrap_birch_param_type__get__cprogpwidth(self._handle)
        
        @cprogpwidth.setter
        def cprogpwidth(self, cprogpwidth):
            _pyclustering.f90wrap_birch_param_type__set__cprogpwidth(self._handle, \
                cprogpwidth)
        
        @property
        def cprogrdepth(self):
            """
            Element cprogrdepth ftype=integer  pytype=int
            
            
            Defined at birch_2wrap.F90 line 20
            
            """
            return _pyclustering.f90wrap_birch_param_type__get__cprogrdepth(self._handle)
        
        @cprogrdepth.setter
        def cprogrdepth(self, cprogrdepth):
            _pyclustering.f90wrap_birch_param_type__set__cprogrdepth(self._handle, \
                cprogrdepth)
        
        def __str__(self):
            ret = ['<birch_param_type>{\n']
            ret.append('    cmaxrad : ')
            ret.append(repr(self.cmaxrad))
            ret.append(',\n    cradius : ')
            ret.append(repr(self.cradius))
            ret.append(',\n    c_nhier : ')
            ret.append(repr(self.c_nhier))
            ret.append(',\n    cdis_crit : ')
            ret.append(repr(self.cdis_crit))
            ret.append(',\n    c_multires : ')
            ret.append(repr(self.c_multires))
            ret.append(',\n    dopix : ')
            ret.append(repr(self.dopix))
            ret.append(',\n    doheuristic : ')
            ret.append(repr(self.doheuristic))
            ret.append(',\n    good_pigs : ')
            ret.append(repr(self.good_pigs))
            ret.append(',\n    re_freq : ')
            ret.append(repr(self.re_freq))
            ret.append(',\n    cprogindrmax : ')
            ret.append(repr(self.cprogindrmax))
            ret.append(',\n    cprogpwidth : ')
            ret.append(repr(self.cprogpwidth))
            ret.append(',\n    cprogrdepth : ')
            ret.append(repr(self.cprogrdepth))
            ret.append('}')
            return ''.join(ret)
        
        _dt_array_initialisers = []
        
    
    @staticmethod
    def birch_wrapper(self, nrepls, nrepls_sampled, cludata_in, nfeats, nsnaps):
        """
        birch_wrapper(self, nrepls, nrepls_sampled, cludata_in, nfeats, nsnaps)
        
        
        Defined at birch_2wrap.F90 lines 28-230
        
        Parameters
        ----------
        params : birch_param_type
        nrepls : int
        nrepls_sampled : int
        cludata_in : float array
        nfeats : int
        nsnaps : int
        
        """
        _pyclustering.f90wrap_birch_wrapper(params=self._handle, nrepls=nrepls, \
            nrepls_sampled=nrepls_sampled, cludata_in=cludata_in, nfeats=nfeats, \
            nsnaps=nsnaps)
    
    @staticmethod
    def clean_after_fit():
        """
        clean_after_fit()
        
        
        Defined at birch_2wrap.F90 lines 235-270
        
        
        """
        _pyclustering.f90wrap_clean_after_fit()
    
    @staticmethod
    def gen_clu_out(clu_out, nsnaps, nrefine):
        """
        gen_clu_out(clu_out, nsnaps, nrefine)
        
        
        Defined at birch_2wrap.F90 lines 275-319
        
        Parameters
        ----------
        clu_out : float array
        nsnaps : int
        nrefine : int
        
        """
        _pyclustering.f90wrap_gen_clu_out(clu_out=clu_out, nsnaps=nsnaps, \
            nrefine=nrefine)
    
    @staticmethod
    def gen_clu_statistics(nsnaps_by_clu, centroids, radii, diameters, nclus):
        """
        gen_clu_statistics(nsnaps_by_clu, centroids, radii, diameters, nclus)
        
        
        Defined at birch_2wrap.F90 lines 322-345
        
        Parameters
        ----------
        nsnaps_by_clu : int array
        centroids : int array
        radii : float array
        diameters : float array
        nclus : int
        
        """
        _pyclustering.f90wrap_gen_clu_statistics(nsnaps_by_clu=nsnaps_by_clu, \
            centroids=centroids, radii=radii, diameters=diameters, nclus=nclus)
    
    @staticmethod
    def get_progind(progind_out_aux, nsnaps, dummy):
        """
        get_progind(progind_out_aux, nsnaps, dummy)
        
        
        Defined at birch_2wrap.F90 lines 348-361
        
        Parameters
        ----------
        progind_out_aux : float array
        nsnaps : int
        dummy : int
        
        """
        _pyclustering.f90wrap_get_progind(progind_out_aux=progind_out_aux, \
            nsnaps=nsnaps, dummy=dummy)
    
    @staticmethod
    def get_nmap(nmap_out_aux, nrepls_sampled):
        """
        get_nmap(nmap_out_aux, nrepls_sampled)
        
        
        Defined at birch_2wrap.F90 lines 363-374
        
        Parameters
        ----------
        nmap_out_aux : int array
        nrepls_sampled : int
        
        """
        _pyclustering.f90wrap_get_nmap(nmap_out_aux=nmap_out_aux, \
            nrepls_sampled=nrepls_sampled)
    
    @staticmethod
    def give_me_an_int():
        """
        give_me_an_int = give_me_an_int()
        
        
        Defined at birch_2wrap.F90 lines 376-381
        
        
        Returns
        -------
        give_me_an_int : int
        
        """
        give_me_an_int = _pyclustering.f90wrap_give_me_an_int()
        return give_me_an_int
    
    @staticmethod
    def do_heuristic(istep, tpi, nrepls_sampled):
        """
        do_heuristic(istep, tpi, nrepls_sampled)
        
        
        Defined at birch_2wrap.F90 lines 393-552
        
        Parameters
        ----------
        istep : int
        tpi : int
        nrepls_sampled : int
        
        """
        _pyclustering.f90wrap_do_heuristic(istep=istep, tpi=tpi, \
            nrepls_sampled=nrepls_sampled)
    
    @staticmethod
    def print_trace(istep, nmap, nrepls_sampled):
        """
        print_trace(istep, nmap, nrepls_sampled)
        
        
        Defined at birch_2wrap.F90 lines 555-578
        
        Parameters
        ----------
        istep : int
        nmap : int array
        nrepls_sampled : int
        
        """
        _pyclustering.f90wrap_print_trace(istep=istep, nmap=nmap, \
            nrepls_sampled=nrepls_sampled)
    
    _dt_array_initialisers = []
    

pyclustering = Pyclustering()

