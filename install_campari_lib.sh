#!/bin/bash
# helper script to install Campari:

CWD=$(pwd)
INSTALLDIR=${CWD}/inst
mkdir -p ${INSTALLDIR}

# get campari distribution:
wget -nv -O ${INSTALLDIR}/debcampari-master.zip https://gitlab.com/CaflischLab/debcampari/-/archive/master/debcampari-master.zip
# unzip:
unzip -o -qq -d ${INSTALLDIR} ${INSTALLDIR}/debcampari-master.zip 

# configure:
cd ${INSTALLDIR}/debcampari-master/source

${INSTALLDIR}/debcampari-master/source/configure --with-campari-home=${INSTALLDIR}/debcampari-master/ --enable-fast-compilation --with-netcdf4=no --enable-mpi=no --enable-threads=no --with-trailing-user-fcflags=-fPIC
#sed -i "/^FFLAGS.*DEFAULTS\}$/s/-DLINK_XDR//g" Makefile # for the moment remove linking to xdr

# writing the Makefile.local:
MAKEFILE_ADD='
CC=gcc # Intel C compiler for XDR 
XDRFFLAGS=-O3 -funroll-loops -march=native -fPIC 
XDRCFLAGS=-O3 -funroll-loops -march=native -fPIC
HSLFFLAGS=${XDRFFLAGS} 
HSLEXTRALIBS=-lblas'
echo "${MAKEFILE_ADD}" >> Makefile.local 
# -fPIC is necessary to link campari as a library

# renaming output library (to avoid problems with linking afterwards)
sed -i 's/lcampari.a/libcampari.a/g' Makefile

# making:
make clean
make campari
