#!/usr/bin/env python
from __future__ import division, absolute_import, print_function

import os
from numpy.distutils.core import Extension
import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

#def configuration(parent_package='',top_path=None):
#    from numpy.distutils.misc_util import Configuration
#    config = Configuration('pytbc',parent_package,top_path)
#    return config

sourcefile = './src/f90wrap_birch_2wrap.f90'
ext1 = Extension(name="_pyclustering",
                sources=[sourcefile],
                extra_f90_compile_args=["-I./src/build"],
                extra_link_args=["-L./src/build",
                                 "-L./inst/debcampari-master/lib/x86_64",
                                 "-lcampari",
                                 "-lxdrf",
                                 "-lfftw3",
                                 "-llapack",
                                 ],
                extra_objects=["./src/build/birch_2wrap.o",
                               "./src/build/mod_binding_helper.o"]
                )


if __name__ == "__main__":
    from numpy.distutils.core import setup
    #setup(**configuration(top_path='').todict())
    #setup(configuration=configuration)
    setup(name = 'pytbc',
          version = '0.0.0',
          license = 'GPLv3',
          description = "Tree-based clustering bindings",
          long_description = long_description,
          keywords = "clustering molecular dynamics",
          url = "https://gitlab.com/clangi/pytbc",
          author = "Cassiano Langini",
          author_email = "cassiano.langini@gmail.com",
          packages = setuptools.find_packages(),
          install_requires = ['numpy'],
          tests_require=['pytest', 
                         'pytest-cov',
                         'numpy',
                         'sklearn'
                        ],
          classifiers=[
              "Programming Language :: Python :: 3",
          ],
          ext_modules = [ext1]
          )
