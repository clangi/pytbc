#!/bin/bash

mkdir -p build

FFLAGS="-I. -L. -I../inst/debcampari-master/lib/x86_64 -I../inst/debcampari-master/source -L/usr/lib/x86_64-linux-gnu -L../inst/debcampari-master/lib/x86_64 -lfftw3 -lfftw3_threads -llapack -lcampari -lxdrf"

# making modules
gfortran ${FFLAGS} -fPIC -c mod_binding_helper.F90 -J build -o build/mod_binding_helper.o
gfortran ${FFLAGS} -fPIC -c birch_2wrap.F90 -J build -o build/birch_2wrap.o

# rewriting f90wrapped code
f90wrap -m pyclustering birch_2wrap.F90 
sed -i 's/real(4)/real(8)/g' f90wrap_birch_2wrap.f90
sed -i 's/integer(4)/integer(8)/g' f90wrap_birch_2wrap.f90

sed -i 's/Birch_Param_Type/birch_param_type/g' pyclustering.py
mv pyclustering.py ../pytbc/.
