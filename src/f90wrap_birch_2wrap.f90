! Module pyclustering defined in file birch_2wrap.F90

subroutine f90wrap_birch_param_type__get__cmaxrad(this, f90wrap_cmaxrad)
    use pyclustering, only: birch_param_type
    implicit none
    type birch_param_type_ptr_type
        type(birch_param_type), pointer :: p => NULL()
    end type birch_param_type_ptr_type
    integer, intent(in)   :: this(2)
    type(birch_param_type_ptr_type) :: this_ptr
    real(8), intent(out) :: f90wrap_cmaxrad
    
    this_ptr = transfer(this, this_ptr)
    f90wrap_cmaxrad = this_ptr%p%cmaxrad
end subroutine f90wrap_birch_param_type__get__cmaxrad

subroutine f90wrap_birch_param_type__set__cmaxrad(this, f90wrap_cmaxrad)
    use pyclustering, only: birch_param_type
    implicit none
    type birch_param_type_ptr_type
        type(birch_param_type), pointer :: p => NULL()
    end type birch_param_type_ptr_type
    integer, intent(in)   :: this(2)
    type(birch_param_type_ptr_type) :: this_ptr
    real(8), intent(in) :: f90wrap_cmaxrad
    
    this_ptr = transfer(this, this_ptr)
    this_ptr%p%cmaxrad = f90wrap_cmaxrad
end subroutine f90wrap_birch_param_type__set__cmaxrad

subroutine f90wrap_birch_param_type__get__cradius(this, f90wrap_cradius)
    use pyclustering, only: birch_param_type
    implicit none
    type birch_param_type_ptr_type
        type(birch_param_type), pointer :: p => NULL()
    end type birch_param_type_ptr_type
    integer, intent(in)   :: this(2)
    type(birch_param_type_ptr_type) :: this_ptr
    real(8), intent(out) :: f90wrap_cradius
    
    this_ptr = transfer(this, this_ptr)
    f90wrap_cradius = this_ptr%p%cradius
end subroutine f90wrap_birch_param_type__get__cradius

subroutine f90wrap_birch_param_type__set__cradius(this, f90wrap_cradius)
    use pyclustering, only: birch_param_type
    implicit none
    type birch_param_type_ptr_type
        type(birch_param_type), pointer :: p => NULL()
    end type birch_param_type_ptr_type
    integer, intent(in)   :: this(2)
    type(birch_param_type_ptr_type) :: this_ptr
    real(8), intent(in) :: f90wrap_cradius
    
    this_ptr = transfer(this, this_ptr)
    this_ptr%p%cradius = f90wrap_cradius
end subroutine f90wrap_birch_param_type__set__cradius

subroutine f90wrap_birch_param_type__get__c_nhier(this, f90wrap_c_nhier)
    use pyclustering, only: birch_param_type
    implicit none
    type birch_param_type_ptr_type
        type(birch_param_type), pointer :: p => NULL()
    end type birch_param_type_ptr_type
    integer, intent(in)   :: this(2)
    type(birch_param_type_ptr_type) :: this_ptr
    integer, intent(out) :: f90wrap_c_nhier
    
    this_ptr = transfer(this, this_ptr)
    f90wrap_c_nhier = this_ptr%p%c_nhier
end subroutine f90wrap_birch_param_type__get__c_nhier

subroutine f90wrap_birch_param_type__set__c_nhier(this, f90wrap_c_nhier)
    use pyclustering, only: birch_param_type
    implicit none
    type birch_param_type_ptr_type
        type(birch_param_type), pointer :: p => NULL()
    end type birch_param_type_ptr_type
    integer, intent(in)   :: this(2)
    type(birch_param_type_ptr_type) :: this_ptr
    integer, intent(in) :: f90wrap_c_nhier
    
    this_ptr = transfer(this, this_ptr)
    this_ptr%p%c_nhier = f90wrap_c_nhier
end subroutine f90wrap_birch_param_type__set__c_nhier

subroutine f90wrap_birch_param_type__get__cdis_crit(this, f90wrap_cdis_crit)
    use pyclustering, only: birch_param_type
    implicit none
    type birch_param_type_ptr_type
        type(birch_param_type), pointer :: p => NULL()
    end type birch_param_type_ptr_type
    integer, intent(in)   :: this(2)
    type(birch_param_type_ptr_type) :: this_ptr
    integer, intent(out) :: f90wrap_cdis_crit
    
    this_ptr = transfer(this, this_ptr)
    f90wrap_cdis_crit = this_ptr%p%cdis_crit
end subroutine f90wrap_birch_param_type__get__cdis_crit

subroutine f90wrap_birch_param_type__set__cdis_crit(this, f90wrap_cdis_crit)
    use pyclustering, only: birch_param_type
    implicit none
    type birch_param_type_ptr_type
        type(birch_param_type), pointer :: p => NULL()
    end type birch_param_type_ptr_type
    integer, intent(in)   :: this(2)
    type(birch_param_type_ptr_type) :: this_ptr
    integer, intent(in) :: f90wrap_cdis_crit
    
    this_ptr = transfer(this, this_ptr)
    this_ptr%p%cdis_crit = f90wrap_cdis_crit
end subroutine f90wrap_birch_param_type__set__cdis_crit

subroutine f90wrap_birch_param_type__get__c_multires(this, f90wrap_c_multires)
    use pyclustering, only: birch_param_type
    implicit none
    type birch_param_type_ptr_type
        type(birch_param_type), pointer :: p => NULL()
    end type birch_param_type_ptr_type
    integer, intent(in)   :: this(2)
    type(birch_param_type_ptr_type) :: this_ptr
    integer, intent(out) :: f90wrap_c_multires
    
    this_ptr = transfer(this, this_ptr)
    f90wrap_c_multires = this_ptr%p%c_multires
end subroutine f90wrap_birch_param_type__get__c_multires

subroutine f90wrap_birch_param_type__set__c_multires(this, f90wrap_c_multires)
    use pyclustering, only: birch_param_type
    implicit none
    type birch_param_type_ptr_type
        type(birch_param_type), pointer :: p => NULL()
    end type birch_param_type_ptr_type
    integer, intent(in)   :: this(2)
    type(birch_param_type_ptr_type) :: this_ptr
    integer, intent(in) :: f90wrap_c_multires
    
    this_ptr = transfer(this, this_ptr)
    this_ptr%p%c_multires = f90wrap_c_multires
end subroutine f90wrap_birch_param_type__set__c_multires

subroutine f90wrap_birch_param_type__get__dopix(this, f90wrap_dopix)
    use pyclustering, only: birch_param_type
    implicit none
    type birch_param_type_ptr_type
        type(birch_param_type), pointer :: p => NULL()
    end type birch_param_type_ptr_type
    integer, intent(in)   :: this(2)
    type(birch_param_type_ptr_type) :: this_ptr
    logical, intent(out) :: f90wrap_dopix
    
    this_ptr = transfer(this, this_ptr)
    f90wrap_dopix = this_ptr%p%dopix
end subroutine f90wrap_birch_param_type__get__dopix

subroutine f90wrap_birch_param_type__set__dopix(this, f90wrap_dopix)
    use pyclustering, only: birch_param_type
    implicit none
    type birch_param_type_ptr_type
        type(birch_param_type), pointer :: p => NULL()
    end type birch_param_type_ptr_type
    integer, intent(in)   :: this(2)
    type(birch_param_type_ptr_type) :: this_ptr
    logical, intent(in) :: f90wrap_dopix
    
    this_ptr = transfer(this, this_ptr)
    this_ptr%p%dopix = f90wrap_dopix
end subroutine f90wrap_birch_param_type__set__dopix

subroutine f90wrap_birch_param_type__get__doheuristic(this, f90wrap_doheuristic)
    use pyclustering, only: birch_param_type
    implicit none
    type birch_param_type_ptr_type
        type(birch_param_type), pointer :: p => NULL()
    end type birch_param_type_ptr_type
    integer, intent(in)   :: this(2)
    type(birch_param_type_ptr_type) :: this_ptr
    logical, intent(out) :: f90wrap_doheuristic
    
    this_ptr = transfer(this, this_ptr)
    f90wrap_doheuristic = this_ptr%p%doheuristic
end subroutine f90wrap_birch_param_type__get__doheuristic

subroutine f90wrap_birch_param_type__set__doheuristic(this, f90wrap_doheuristic)
    use pyclustering, only: birch_param_type
    implicit none
    type birch_param_type_ptr_type
        type(birch_param_type), pointer :: p => NULL()
    end type birch_param_type_ptr_type
    integer, intent(in)   :: this(2)
    type(birch_param_type_ptr_type) :: this_ptr
    logical, intent(in) :: f90wrap_doheuristic
    
    this_ptr = transfer(this, this_ptr)
    this_ptr%p%doheuristic = f90wrap_doheuristic
end subroutine f90wrap_birch_param_type__set__doheuristic

subroutine f90wrap_birch_param_type__get__good_pigs(this, f90wrap_good_pigs)
    use pyclustering, only: birch_param_type
    implicit none
    type birch_param_type_ptr_type
        type(birch_param_type), pointer :: p => NULL()
    end type birch_param_type_ptr_type
    integer, intent(in)   :: this(2)
    type(birch_param_type_ptr_type) :: this_ptr
    integer, intent(out) :: f90wrap_good_pigs
    
    this_ptr = transfer(this, this_ptr)
    f90wrap_good_pigs = this_ptr%p%good_pigs
end subroutine f90wrap_birch_param_type__get__good_pigs

subroutine f90wrap_birch_param_type__set__good_pigs(this, f90wrap_good_pigs)
    use pyclustering, only: birch_param_type
    implicit none
    type birch_param_type_ptr_type
        type(birch_param_type), pointer :: p => NULL()
    end type birch_param_type_ptr_type
    integer, intent(in)   :: this(2)
    type(birch_param_type_ptr_type) :: this_ptr
    integer, intent(in) :: f90wrap_good_pigs
    
    this_ptr = transfer(this, this_ptr)
    this_ptr%p%good_pigs = f90wrap_good_pigs
end subroutine f90wrap_birch_param_type__set__good_pigs

subroutine f90wrap_birch_param_type__get__re_freq(this, f90wrap_re_freq)
    use pyclustering, only: birch_param_type
    implicit none
    type birch_param_type_ptr_type
        type(birch_param_type), pointer :: p => NULL()
    end type birch_param_type_ptr_type
    integer, intent(in)   :: this(2)
    type(birch_param_type_ptr_type) :: this_ptr
    integer, intent(out) :: f90wrap_re_freq
    
    this_ptr = transfer(this, this_ptr)
    f90wrap_re_freq = this_ptr%p%re_freq
end subroutine f90wrap_birch_param_type__get__re_freq

subroutine f90wrap_birch_param_type__set__re_freq(this, f90wrap_re_freq)
    use pyclustering, only: birch_param_type
    implicit none
    type birch_param_type_ptr_type
        type(birch_param_type), pointer :: p => NULL()
    end type birch_param_type_ptr_type
    integer, intent(in)   :: this(2)
    type(birch_param_type_ptr_type) :: this_ptr
    integer, intent(in) :: f90wrap_re_freq
    
    this_ptr = transfer(this, this_ptr)
    this_ptr%p%re_freq = f90wrap_re_freq
end subroutine f90wrap_birch_param_type__set__re_freq

subroutine f90wrap_birch_param_type__get__cprogindrmax(this, f90wrap_cprogindrmax)
    use pyclustering, only: birch_param_type
    implicit none
    type birch_param_type_ptr_type
        type(birch_param_type), pointer :: p => NULL()
    end type birch_param_type_ptr_type
    integer, intent(in)   :: this(2)
    type(birch_param_type_ptr_type) :: this_ptr
    integer, intent(out) :: f90wrap_cprogindrmax
    
    this_ptr = transfer(this, this_ptr)
    f90wrap_cprogindrmax = this_ptr%p%cprogindrmax
end subroutine f90wrap_birch_param_type__get__cprogindrmax

subroutine f90wrap_birch_param_type__set__cprogindrmax(this, f90wrap_cprogindrmax)
    use pyclustering, only: birch_param_type
    implicit none
    type birch_param_type_ptr_type
        type(birch_param_type), pointer :: p => NULL()
    end type birch_param_type_ptr_type
    integer, intent(in)   :: this(2)
    type(birch_param_type_ptr_type) :: this_ptr
    integer, intent(in) :: f90wrap_cprogindrmax
    
    this_ptr = transfer(this, this_ptr)
    this_ptr%p%cprogindrmax = f90wrap_cprogindrmax
end subroutine f90wrap_birch_param_type__set__cprogindrmax

subroutine f90wrap_birch_param_type__get__cprogpwidth(this, f90wrap_cprogpwidth)
    use pyclustering, only: birch_param_type
    implicit none
    type birch_param_type_ptr_type
        type(birch_param_type), pointer :: p => NULL()
    end type birch_param_type_ptr_type
    integer, intent(in)   :: this(2)
    type(birch_param_type_ptr_type) :: this_ptr
    integer, intent(out) :: f90wrap_cprogpwidth
    
    this_ptr = transfer(this, this_ptr)
    f90wrap_cprogpwidth = this_ptr%p%cprogpwidth
end subroutine f90wrap_birch_param_type__get__cprogpwidth

subroutine f90wrap_birch_param_type__set__cprogpwidth(this, f90wrap_cprogpwidth)
    use pyclustering, only: birch_param_type
    implicit none
    type birch_param_type_ptr_type
        type(birch_param_type), pointer :: p => NULL()
    end type birch_param_type_ptr_type
    integer, intent(in)   :: this(2)
    type(birch_param_type_ptr_type) :: this_ptr
    integer, intent(in) :: f90wrap_cprogpwidth
    
    this_ptr = transfer(this, this_ptr)
    this_ptr%p%cprogpwidth = f90wrap_cprogpwidth
end subroutine f90wrap_birch_param_type__set__cprogpwidth

subroutine f90wrap_birch_param_type__get__cprogrdepth(this, f90wrap_cprogrdepth)
    use pyclustering, only: birch_param_type
    implicit none
    type birch_param_type_ptr_type
        type(birch_param_type), pointer :: p => NULL()
    end type birch_param_type_ptr_type
    integer, intent(in)   :: this(2)
    type(birch_param_type_ptr_type) :: this_ptr
    integer, intent(out) :: f90wrap_cprogrdepth
    
    this_ptr = transfer(this, this_ptr)
    f90wrap_cprogrdepth = this_ptr%p%cprogrdepth
end subroutine f90wrap_birch_param_type__get__cprogrdepth

subroutine f90wrap_birch_param_type__set__cprogrdepth(this, f90wrap_cprogrdepth)
    use pyclustering, only: birch_param_type
    implicit none
    type birch_param_type_ptr_type
        type(birch_param_type), pointer :: p => NULL()
    end type birch_param_type_ptr_type
    integer, intent(in)   :: this(2)
    type(birch_param_type_ptr_type) :: this_ptr
    integer, intent(in) :: f90wrap_cprogrdepth
    
    this_ptr = transfer(this, this_ptr)
    this_ptr%p%cprogrdepth = f90wrap_cprogrdepth
end subroutine f90wrap_birch_param_type__set__cprogrdepth

subroutine f90wrap_birch_param_type_initialise(this)
    use pyclustering, only: birch_param_type
    implicit none
    
    type birch_param_type_ptr_type
        type(birch_param_type), pointer :: p => NULL()
    end type birch_param_type_ptr_type
    type(birch_param_type_ptr_type) :: this_ptr
    integer, intent(out), dimension(2) :: this
    allocate(this_ptr%p)
    this = transfer(this_ptr, this)
end subroutine f90wrap_birch_param_type_initialise

subroutine f90wrap_birch_param_type_finalise(this)
    use pyclustering, only: birch_param_type
    implicit none
    
    type birch_param_type_ptr_type
        type(birch_param_type), pointer :: p => NULL()
    end type birch_param_type_ptr_type
    type(birch_param_type_ptr_type) :: this_ptr
    integer, intent(in), dimension(2) :: this
    this_ptr = transfer(this, this_ptr)
    deallocate(this_ptr%p)
end subroutine f90wrap_birch_param_type_finalise

subroutine f90wrap_birch_wrapper(params, nrepls, nrepls_sampled, cludata_in, nfeats, nsnaps, n0, n1)
    use pyclustering, only: birch_wrapper, birch_param_type
    implicit none
    
    type birch_param_type_ptr_type
        type(birch_param_type), pointer :: p => NULL()
    end type birch_param_type_ptr_type
    type(birch_param_type_ptr_type) :: params_ptr
    integer, intent(in), dimension(2) :: params
    integer, intent(in) :: nrepls
    integer, intent(in) :: nrepls_sampled
    real(8), intent(in), dimension(n0,n1) :: cludata_in
    integer, intent(in) :: nfeats
    integer, intent(in) :: nsnaps
    integer :: n0
    !f2py intent(hide), depend(cludata_in) :: n0 = shape(cludata_in,0)
    integer :: n1
    !f2py intent(hide), depend(cludata_in) :: n1 = shape(cludata_in,1)
    params_ptr = transfer(params, params_ptr)
    call birch_wrapper(params=params_ptr%p, nrepls=nrepls, nrepls_sampled=nrepls_sampled, cludata_in=cludata_in, &
        nfeats=nfeats, nsnaps=nsnaps)
end subroutine f90wrap_birch_wrapper

subroutine f90wrap_clean_after_fit
    use pyclustering, only: clean_after_fit
    implicit none
    
    call clean_after_fit()
end subroutine f90wrap_clean_after_fit

subroutine f90wrap_gen_clu_out(clu_out, nsnaps, nrefine, n0, n1)
    use pyclustering, only: gen_clu_out
    implicit none
    
    real(8), intent(inout), dimension(n0,n1) :: clu_out
    integer, intent(in) :: nsnaps
    integer, intent(in) :: nrefine
    integer :: n0
    !f2py intent(hide), depend(clu_out) :: n0 = shape(clu_out,0)
    integer :: n1
    !f2py intent(hide), depend(clu_out) :: n1 = shape(clu_out,1)
    call gen_clu_out(clu_out=clu_out, nsnaps=nsnaps, nrefine=nrefine)
end subroutine f90wrap_gen_clu_out

subroutine f90wrap_gen_clu_statistics(nsnaps_by_clu, centroids, radii, diameters, nclus, n0, n1, n2, n3)
    use pyclustering, only: gen_clu_statistics
    implicit none
    
    integer, intent(inout), dimension(n0) :: nsnaps_by_clu
    integer, intent(inout), dimension(n1) :: centroids
    real(8), intent(inout), dimension(n2) :: radii
    real(8), intent(inout), dimension(n3) :: diameters
    integer, intent(in) :: nclus
    integer :: n0
    !f2py intent(hide), depend(nsnaps_by_clu) :: n0 = shape(nsnaps_by_clu,0)
    integer :: n1
    !f2py intent(hide), depend(centroids) :: n1 = shape(centroids,0)
    integer :: n2
    !f2py intent(hide), depend(radii) :: n2 = shape(radii,0)
    integer :: n3
    !f2py intent(hide), depend(diameters) :: n3 = shape(diameters,0)
    call gen_clu_statistics(nsnaps_by_clu=nsnaps_by_clu, centroids=centroids, radii=radii, diameters=diameters, nclus=nclus)
end subroutine f90wrap_gen_clu_statistics

subroutine f90wrap_get_progind(progind_out_aux, nsnaps, dummy, n0, n1)
    use pyclustering, only: get_progind
    implicit none
    
    real(8), intent(inout), dimension(n0,n1) :: progind_out_aux
    integer, intent(in) :: nsnaps
    integer, intent(in) :: dummy
    integer :: n0
    !f2py intent(hide), depend(progind_out_aux) :: n0 = shape(progind_out_aux,0)
    integer :: n1
    !f2py intent(hide), depend(progind_out_aux) :: n1 = shape(progind_out_aux,1)
    call get_progind(progind_out_aux=progind_out_aux, nsnaps=nsnaps, dummy=dummy)
end subroutine f90wrap_get_progind

subroutine f90wrap_get_nmap(nmap_out_aux, nrepls_sampled, n0)
    use pyclustering, only: get_nmap
    implicit none
    
    integer, intent(inout), dimension(n0) :: nmap_out_aux
    integer, intent(in) :: nrepls_sampled
    integer :: n0
    !f2py intent(hide), depend(nmap_out_aux) :: n0 = shape(nmap_out_aux,0)
    call get_nmap(nmap_out_aux=nmap_out_aux, nrepls_sampled=nrepls_sampled)
end subroutine f90wrap_get_nmap

subroutine f90wrap_give_me_an_int(ret_give_me_an_int)
    use pyclustering, only: give_me_an_int
    implicit none
    
    integer, intent(out) :: ret_give_me_an_int
    ret_give_me_an_int = give_me_an_int()
end subroutine f90wrap_give_me_an_int

subroutine f90wrap_do_heuristic(istep, tpi, nrepls_sampled)
    use pyclustering, only: do_heuristic
    implicit none
    
    integer(8), intent(in) :: istep
    integer, intent(in) :: tpi
    integer, intent(in) :: nrepls_sampled
    call do_heuristic(istep=istep, tpi=tpi, nrepls_sampled=nrepls_sampled)
end subroutine f90wrap_do_heuristic

subroutine f90wrap_print_trace(istep, nmap, nrepls_sampled, n0)
    use pyclustering, only: print_trace
    implicit none
    
    integer(8), intent(in) :: istep
    integer, dimension(n0) :: nmap
    integer, intent(in) :: nrepls_sampled
    integer :: n0
    !f2py intent(hide), depend(nmap) :: n0 = shape(nmap,0)
    call print_trace(istep=istep, nmap=nmap, nrepls_sampled=nrepls_sampled)
end subroutine f90wrap_print_trace

! End of module pyclustering defined in file birch_2wrap.F90

