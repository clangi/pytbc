FROM python:3.7-buster

WORKDIR /app

COPY requirements.txt ./

RUN apt-get update && apt-get install -y \
    apt-utils \
    gfortran \
    libblas-dev \
    liblapack-dev \
    libfftw3-dev \
    wget

RUN pip install --upgrade pip && \
    pip install --no-cache-dir -r requirements.txt && \
    pip install --no-cache-dir f90wrap

RUN wget -nv -O pandoc-2.9.2-linux-amd64.tar.gz  https://github.com/jgm/pandoc/releases/download/2.9.2/pandoc-2.9.2-linux-amd64.tar.gz && \
    tar -xvzf pandoc-2.9.2-linux-amd64.tar.gz --strip-components 1 -C /usr/local/

    
