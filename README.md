# pytbc - Tree-based clustering in Python

**pytbc** contains Python bindings to the tree-based clustering algorithm 
by Vitalis and Caflisch [1] implemented in [Campari](http://campari.sourceforge.net/).

The Fortran to Python interface is generated with [f90wrap](https://github.com/jameskermode/f90wrap) 
to have access to derived types and then compiled with [f2py](https://docs.scipy.org/doc/numpy/f2py/).

------------------------------------------------------------------------
### Documentation ###

The documentation of this project is available at [https://clangi.gitlab.io/pytbc](https://clangi.gitlab.io/pytbc).

------------------------------------------------------------------------
### Installation ###

**pytbc** has to be linked to a valid Campari installation. The easiest way to 
install Campari is by using the configuration file distributed at
[https://gitlab.com/CaflischLab/debcampari](https://gitlab.com/CaflischLab/debcampari). 
Note that the Campari library has to be compiled with flag `-fPIC` to be able to be 
linked to successfully.

We provide a script to run the necessary installation steps:
```sh
git clone https://gitlab.com/clangi/pytbc
cd pytbc
bash install_campari_lib.sh
```
The script installs Campari in the repository folder `./pytbc/inst/` (default).
Campari compilation will generate the static library `libcampari.a` to which 
pytbc is linked. The path to the library should be in the LD\_LIBRARY\_PATH or should
be added to `setup.py` among the `extra_link_args` (already present if Campari
is compiled in the default repository folder).

The Fortran-Python interface (`pyclustering.py`) has been preemptively generated 
with f90wrap. Upon modifications to Fortran source files, it can be regenerated as follows:
```sh
cd src
bash wrapit_f90wrap.sh
```
Once again `libcampari.a` should be in the LD\_LIBRARY\_PATH.

Upon successfull installation of Campari, the Python package can be installed 
with pip:
```sh
pip install -e .
```

------------------------------------------------------------------------
### Usage ###

In order to use **pytbc** it is enough to import it in Python:
```python
import pytbc
```

A Python notebook with example use cases is provided.

------------------------------------------------------------------------
### Extensions ###
Campari **pytbc** bindings are for the moment limited to the tree-based clustering 
algorithm with dihedral or Euclidean distance (see doc). More advanced and complete
functionalities can be accessed by directly running [Campari](http://campari.sourceforge.net/) 
or by using the [CampaRi R package](https://gitlab.com/CaflischLab/CampaRi), a 
complete wrapper of Campari written in R. The user can refer to the documentation
of these projects for more in-depth explanations of the algorithm and use cases.

------------------------------------------------------------------------
### Citations ###
Please cite [1] if you use **pytbc** in your work.
This project was realized as part of an [E-CAM](https://www.e-cam2020.eu/) 
Extended Software Development workshop.
Contributions to this project were given by C. Langini, M. Bacci, D. Garolini and 
A. Vitalis from the [Caflisch lab](http://www.biochem-caflisch.uzh.ch/).



[1] A. Vitalis and A. Caflisch. Efficient Construction of Mesostate Networks from Molecular Dynamics Trajectories. 
J. Chem. Theory Comput. 8 (3), 1108-1120 (2012) [DOI](https://pubs.acs.org/doi/abs/10.1021/ct200801b)
